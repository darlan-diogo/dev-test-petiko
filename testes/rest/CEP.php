<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{
	protected static $base_url = "https://api.postmon.com.br/v1/cep";
    public static function getAddressByCep($cep)
    {
		try{
			$result = file_get_contents(self::$base_url.'/'.$cep);
			$result = json_decode($result,true);
			$result = json_encode($result, JSON_PRETTY_PRINT);
			return $result;
		} catch (Exception $e) {
			echo 'Exception: '. $e->getMessage();
		}
    }
}

$cep =  '21512002';

var_dump(CEP::getAddressByCep($cep));