// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  try {
    if(obj.hasOwnProperty(prop)){
      delete obj[prop];
      return true;
    }
    return false; 
  } catch (error) {
    console.error(error)
  }
}

let obj = { name: "Darlan", age: "28", height: "1.62" };

console.log(removeProperty(obj, "age"));